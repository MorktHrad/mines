#! /bin/sh

clear

MY_DIR=$HOME/Scripts/minesweeper

cd $MY_DIR

gcc src/main.c -o build/minesweeper -lX11 -lGL -lGLU -lglut -pthread

if [ "$?" == 0 ]; then
  echo "Compiled correctly!"
  printf "\n./build/minesweeper "
  read input
  build/minesweeper $input
else
  echo "Error compiling..."
fi


