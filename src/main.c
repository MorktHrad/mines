#include <stdio.h>
#include <stdlib.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/XKBlib.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>
#include <pthread.h>

#define DEBUG = 1

// Declaring functions
void spawn_window();
void run_app();
void set_camera();
void expose_manager();
void key_handler_home();
void button_handler_home();

//Global X / GL Variables
Display                 *dpy;
Window                  root;
XVisualInfo             *vi;
XSetWindowAttributes    swa;
Window                  win;
XWindowAttributes       gwa;
XEvent          xev;
XClassHint *wclass;

GLint                   att[] = {GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None};
Colormap                cmap;
GLXContext              glc;

//Camera vectors
float pos[3];
float center[3];
float up[3];
float ortho[4];

//Thread variables
int gl_occupied = 0;
pthread_cond_t gl_cond;
pthread_mutex_t mutex;

//X/GL Functions
void spawn_window(){
  dpy = XOpenDisplay(NULL);

    if (dpy == NULL){
      printf("Cannot connect to X server\n");
      exit(0);
    }

  root = DefaultRootWindow(dpy);
  vi = glXChooseVisual(dpy, 0, att);

  if (vi == NULL){
    printf("No appropiate visual found\n");
    exit(0);
  }

  #ifdef DEBUG
  else{
    printf("Visual %p selected\n", (void*)vi->visualid);
  }
  #endif

  cmap = XCreateColormap(dpy, root, vi->visual, AllocNone);
  swa.colormap = cmap;
  swa.event_mask = ExposureMask | KeyPressMask | ButtonPressMask;
  win = XCreateWindow(dpy, root, 0, 0, 800, 450, 0,
                      vi->depth, InputOutput, vi->visual,
                      CWColormap | CWEventMask, &swa);
  XMapWindow(dpy, win);
  XFlush(dpy);

  wclass = XAllocClassHint();
  wclass->res_name = "Mines";
  wclass->res_class= "mines";

  XStoreName(dpy, win, "Mines");
  XSetClassHint(dpy, win, wclass);
  glc = glXCreateContext(dpy, vi, NULL, GL_TRUE);
  glXMakeCurrent(dpy, win, glc);
}

void set_camera(){
  pos[0] = 0;
  pos[1] = 0;
  pos[2] = 1;
  center[0] = 0;
  center[1] = 0;
  center[2] = 0;
  ortho[0] = 0;
  ortho[1] = gwa.width;
  ortho[2] = 0;
  ortho[3] = gwa.height;
}

void expose_manager(){
  //Flush screen
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  //Get new screen size
  XGetWindowAttributes(dpy, win, &gwa);
  glViewport(0, 0, gwa.width, gwa.height);

  //Ortho transform related to screen dimensions
  ortho[0] = 0;
  ortho[1] = gwa.width;
  ortho[2] = 0;
  ortho[3] = gwa.height;

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(ortho[0], ortho[1], ortho[2], ortho[3], 0, 2);
  gluLookAt(pos[0],pos[1],pos[2], center[0],center[1],center[2], up[0],up[1],up[2]);

  #ifdef DEBUG
  printf("\nExposure event. New geometry: (%d, %d)\n", gwa.height, gwa.width);
  printf("Orthographic transformation: (%f, %f, %f, %f)\n\n", ortho[0], ortho[1], ortho[2], ortho[3]);
  #endif

  glXSwapBuffers(dpy, win);
  glFinish();
}

void run_app(){
  spawn_window();

  glEnable(GL_DEPTH_TEST);

  glClearColor(0.0, 0.0, 0.0, 0.0);

  set_camera();

  while(1){   //Main loop
    XNextEvent(dpy, &xev);

    if (gl_occupied == 1) {
      pthread_cond_wait(&gl_cond, &mutex);
    }

    gl_occupied = 1;
    XLockDisplay(dpy);
    glXMakeCurrent(dpy, win, glc);

    if (xev.type == Expose){
      expose_manager();
    }

    //Key management
    else if (xev.type == KeyPress){
      #ifdef DEBUG
      printf("KeyPress event: %d, %s\n",
              xev.xkey.keycode, XKeysymToString(
              XkbKeycodeToKeysym(dpy, xev.xkey.keycode,
              0,
              xev.xkey.state & ShiftMask ? 1 : 0)));
      #endif

      key_handler_home();
    }

    XUnlockDisplay(dpy);
    gl_occupied = 0;
    pthread_cond_broadcast(&gl_cond);

  }

}

//Button handler functions
void button_handler_home(){
}

void key_handler_home(){
  switch (xev.xkey.keycode){
    case 24:  //q
    case 9:   //scape
      glXMakeCurrent(dpy, None, NULL);
      glXDestroyContext(dpy, glc);
      XDestroyWindow(dpy, win);
      XCloseDisplay(dpy);
      exit(0);
  }
}

int main (int argc, char *argv[]){
  run_app();
}
